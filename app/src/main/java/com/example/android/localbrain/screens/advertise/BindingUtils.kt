package com.example.android.localbrain.screens.advertise

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.android.localbrain.database.Lcontext

@BindingAdapter("contextText")
fun TextView.setQueryText(item: Lcontext) {
    item.let{
        text = item.text
    }
}