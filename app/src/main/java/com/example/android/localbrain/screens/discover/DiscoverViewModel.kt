package com.example.android.localbrain.screens.discover

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DiscoverViewModel: ViewModel() {

    private val _startDiscoveryEvent = MutableLiveData<Boolean>()
    val startDiscoveryEvent: LiveData<Boolean>
        get() = _startDiscoveryEvent

    private val _stopDiscoveryEvent = MutableLiveData<Boolean>()
    val stopDiscoveryEvent: LiveData<Boolean>
        get() = _stopDiscoveryEvent

    fun onStartDiscovery() {
        _startDiscoveryEvent.value = true
    }

    fun discoveryStarted() {
        _startDiscoveryEvent.value = false
    }
}