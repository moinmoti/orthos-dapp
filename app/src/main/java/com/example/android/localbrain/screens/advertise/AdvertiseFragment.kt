package com.example.android.localbrain.screens.advertise


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
//import android.widget.TextView
import android.widget.Toast
import com.example.android.localbrain.R
import com.google.android.gms.nearby.Nearby
import android.content.DialogInterface
//import javax.swing.text.StyleConstants.setIcon
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android.localbrain.Globals
import com.example.android.localbrain.databinding.AdvertiseFragmentBinding
import com.google.android.gms.nearby.connection.*
import com.google.android.gms.nearby.connection.PayloadTransferUpdate
import com.google.android.gms.nearby.connection.Payload
import com.google.android.gms.nearby.connection.PayloadCallback
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.json.JSONObject


class AdvertiseFragment : Fragment() {

    private val globals: Globals = Globals()

    private lateinit var viewModel: AdvertiseViewModel
    private lateinit var binding: AdvertiseFragmentBinding
    private lateinit var connectionsClient: ConnectionsClient

    private val strategy = Strategy.P2P_STAR
    private val serviceId = "com.example.android.localbrain"
    private val nickname = "LocalBrain"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.advertise_fragment,
                container,
                false
        )

        // Get the viewmodel
        viewModel = ViewModelProviders.of(this).get(AdvertiseViewModel::class.java)

        // Set the viewmodel for databinding - this allows the bound layout access to all of the
        // data in the VieWModel
        binding.advertiseViewModel = viewModel

        // Specify the current activity as the lifecycle owner of the binding. This is used so that
        // the binding can observe LiveData updates
        binding.lifecycleOwner = this

        val adapter = AdvertiseAdapter(LcontextListener {
            lcontextId -> Toast.makeText(context, "Lcontext ${lcontextId} Clicked", Toast.LENGTH_LONG).show()
        })

        binding.advertiseViewList.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        binding.advertiseViewList.adapter = adapter

        viewModel.lcontexts.observe(this, Observer { newContextList ->
            Toast.makeText(context, newContextList[0].text, Toast.LENGTH_LONG).show()
            adapter.submitList(newContextList)
        })

        viewModel.startAdvertisingEvent.observe(this, Observer { newVal ->
            if (newVal == true) {
                startAdvertising()
                viewModel.advertisingStarted()
            }
        })

        viewModel.stopAdvertisingEvent.observe(this, Observer { newVal ->
            if (newVal == true) {
                connectionsClient.stopAdvertising()
                connectionsClient.stopAllEndpoints()
                viewModel.advertisingStopped()
            }
        })

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionsClient = Nearby.getConnectionsClient(requireContext())
    }

    override fun onStop() {
        super.onStop()
        connectionsClient.stopAdvertising()
        connectionsClient.stopAllEndpoints()
    }

    private fun startAdvertising() {
        val advertisingOptions = AdvertisingOptions.Builder().setStrategy(strategy).build()
        Log.d("AdvertiseFragment", "before starting")
        connectionsClient.startAdvertising(
                        nickname, serviceId, connectionLifecycleCallback, advertisingOptions)
                .addOnSuccessListener {
                    Log.d("AdvertiseFragment", "Started Advertising...")
//                    Toast.makeText(context, "Advertising...",  Toast.LENGTH_LONG).show()
                }
                .addOnFailureListener { e: Exception ->
                    Log.d("AdvertiseFragment", e.toString())
                }
    }

    private val connectionLifecycleCallback = object : ConnectionLifecycleCallback() {

        override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
            // Automatically accept the connection on both sides.
//            connectionsClient.acceptConnection(endpointId, payloadCallback)
            AlertDialog.Builder(requireContext())
                    .setTitle("Accept connection to " + connectionInfo.endpointName)
                    .setMessage("Confirm the code matches on both devices: " + connectionInfo.authenticationToken)
                    .setPositiveButton(
                            "Accept"
                    ) { dialog: DialogInterface, which: Int ->
                        Log.d("AdvertiseFragment", "The user confirmed, so we can accept the connection")
                        connectionsClient.acceptConnection(endpointId, payloadCallback)
                    }
                    .setNegativeButton(
                            android.R.string.cancel
                    ) { dialog: DialogInterface, which: Int ->
                        Log.d("AdvertiseFragment", "The user canceled, so we should reject the connection")
                        connectionsClient.rejectConnection(endpointId)
                    }
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
        }

        override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
            when (result.status.statusCode) {
                ConnectionsStatusCodes.STATUS_OK -> runBlocking {
                    Log.d("AdvertiseFragment", "We're connected! Can now start sending and receiving data")
                    launch {
                        delay(7000L)
                        val peerList = mutableListOf<String>()
                        for (p in globals.peers) {
                            peerList += p.addr
                        }
                        val jsonObj = JSONObject()
                        jsonObj.put("type", "peerList")
                        jsonObj.put("peerList", peerList.toString())
                        connectionsClient.sendPayload(
                                endpointId,
                                Payload.fromBytes(jsonObj.toString().toByteArray(Charsets.UTF_8))
                        )
                    }

                }
                ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> {
                    Log.d("AdvertiseFragment", "The connection was rejected by one or both sides")
                }
                ConnectionsStatusCodes.STATUS_ERROR -> {
                    Log.d("AdvertiseFragment", "The connection broke before it was able to be accepted")
                }
                else -> {
                    Log.d("AdvertiseFragment", "Unknown status code")
                }
            }
        }

        override fun onDisconnected(endpointId: String) {
            Log.d(
                "AdvertiseFragment",
                "We've been disconnected from this endpoint. No more data can be sent or received"
            )
        }
    }

    private val payloadCallback = object : PayloadCallback() {
        override fun onPayloadReceived(endpointId: String, payload: Payload) {
            val objStr = payload.asBytes()?.toString() ?: "empty object"
            val jsonObj = JSONObject(objStr)
            if (jsonObj.getString("type") == "signature") {
                viewModel.addLcontext(jsonObj)
            } else if (jsonObj.getString("type") == "peer") {
                viewModel.addPeer(jsonObj)
            }
//            Toast.makeText(context, payload.asBytes()?.toString(Charsets.UTF_8), Toast.LENGTH_LONG).show()
        }

        override fun onPayloadTransferUpdate(endpointId: String, update: PayloadTransferUpdate) {
            if (update.status == PayloadTransferUpdate.Status.SUCCESS) {
                Log.d("AdvertiseFragment", "Success Update Received")
            }
        }
    }
}
