package com.example.android.localbrain.screens.advertise

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.localbrain.Globals
import com.example.android.localbrain.database.Lcontext
import com.example.android.localbrain.database.Peer
import org.json.JSONObject

class AdvertiseViewModel: ViewModel() {

    private val globals: Globals = Globals()

    private val _startAdvertisingEvent = MutableLiveData<Boolean>()
    val startAdvertisingEvent: LiveData<Boolean>
        get() = _startAdvertisingEvent

    private val _stopAdvertisingEvent = MutableLiveData<Boolean>()
    val stopAdvertisingEvent: LiveData<Boolean>
        get() = _stopAdvertisingEvent

    private val _lcontexts = MutableLiveData<MutableList<Lcontext>>()
    val  lcontexts: LiveData<MutableList<Lcontext>>
        get() = _lcontexts

    private val templcs = mutableListOf<Lcontext>()

    private var lcontextCounter: Long = 0
    private var peerCounter: Long = 0

    fun onStartAdvertising() {
        _startAdvertisingEvent.value = true
    }

    fun advertisingStarted() {
        _startAdvertisingEvent.value = false
    }

    fun onStopAdvertising() {
        _stopAdvertisingEvent.value = true
    }

    fun advertisingStopped() {
        _stopAdvertisingEvent.value = false
    }

    fun addLcontext(lcObj: JSONObject) {
        val lc = Lcontext(
                lcontextCounter,
                lcObj.getString("address"),
                lcObj.getString("address"),
                lcObj.getString("hash").toByteArray(),
                lcObj.getInt("v"),
                lcObj.getString("r").toByteArray(),
                lcObj.getString("s").toByteArray()
        )
        globals.addPAddr(lc.addr)
        globals.addH(lc.hash)
        globals.addV(lc.v.toBigInteger())
        globals.addR(lc.r)
        globals.addS(lc.s)
        templcs += lc
        _lcontexts.value = templcs
//        Log.d("AdvertiseViewModel", "Context String: $lcString")
//        Log.d("AdvertiseViewModel", "Templcs: $templcs")
        Log.d("AdvertiseViewModel", _lcontexts.value?.get(0)?.text)
        lcontextCounter++
    }

    fun addPeer(peerObj: JSONObject) {
        val peer = Peer(
                peerCounter,
                peerObj.getString("address"),
                peerObj.getString("location").toDouble(),
                peerObj.getString("longitude").toDouble()
        )
        globals.addPeer(peer)
        peerCounter++
    }
}