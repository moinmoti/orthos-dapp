package com.example.android.localbrain.screens.index

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android.localbrain.database.Query
import com.example.android.localbrain.databinding.ListItemQueryBinding

class IndexAdapter(private val myDataset: Array<Query>, val clickListener: QueryListener): RecyclerView.Adapter<IndexAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int = myDataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(clickListener, myDataset[position])
    }

    class ViewHolder private constructor (val binding: ListItemQueryBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(clickListener: QueryListener, item: Query) {
            binding.query = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemQueryBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class QueryListener(val clickListener: (queryId: Long) -> Unit) {
    fun onClick(query: Query) = clickListener(query.id)
}
