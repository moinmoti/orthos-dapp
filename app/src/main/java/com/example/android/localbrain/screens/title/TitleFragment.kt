/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.localbrain.screens.title

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.android.localbrain.Globals
import com.example.android.localbrain.R
import com.example.android.localbrain.databinding.TitleFragmentBinding

/**
 * Fragment for the starting or title screen of the app
 */
class TitleFragment : Fragment() {

//    private val globals: Globals = Globals()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val binding: TitleFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.title_fragment, container, false)
//        if (!globals.PRIVATE_KEY.isBlank()) {
//            Log.d("TitleFragment", "inside")
//            binding.pkeyEdit.visibility = View.GONE
//            binding.submitPkeyButton.visibility = View.GONE
//            binding.loadQueriesButton.visibility = View.VISIBLE
//            binding.submitQueryButton.visibility = View.VISIBLE
//            binding.advertiseButton.visibility = View.VISIBLE
//            binding.discoverButton.visibility = View.VISIBLE
//        }
//        binding.submitPkeyButton.setOnClickListener {
//            globals.setPKey(binding.pkeyEdit.text.toString())
//            binding.pkeyEdit.visibility = View.GONE
//            binding.submitPkeyButton.visibility = View.GONE
//            binding.loadQueriesButton.visibility = View.VISIBLE
//            binding.submitQueryButton.visibility = View.VISIBLE
//            binding.advertiseButton.visibility = View.VISIBLE
//            binding.discoverButton.visibility = View.VISIBLE
//        }
        binding.loadQueriesButton.setOnClickListener {
            findNavController().navigate(TitleFragmentDirections.actionTitleToIndex())
        }
        binding.submitQueryButton.setOnClickListener {
            findNavController().navigate(TitleFragmentDirections.actionTitleToSubmit())
        }
        binding.advertiseButton.setOnClickListener {
            findNavController().navigate(TitleFragmentDirections.actionTitleToAdvertise())
        }
        binding.discoverButton.setOnClickListener {
            findNavController().navigate(TitleFragmentDirections.actionTitleToDiscover())
        }
        return binding.root
    }
}
