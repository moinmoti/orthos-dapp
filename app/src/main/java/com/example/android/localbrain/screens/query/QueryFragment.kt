/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.localbrain.screens.query

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.example.android.localbrain.R
import com.example.android.localbrain.databinding.QueryFragmentBinding

class QueryFragment : Fragment() {

    private lateinit var viewModel: QueryViewModel
    private lateinit var viewModelFactory: QueryViewModelFactory

    private lateinit var binding: QueryFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.query_fragment,
                container,
                false
        )
        val args = QueryFragmentArgs.fromBundle(arguments!!)
        viewModelFactory = QueryViewModelFactory(args.queryId)
        // Get the viewmodel
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(QueryViewModel::class.java)

        // Set the viewmodel for databinding - this allows the bound layout access to all of the
        // data in the VieWModel
        binding.queryViewModel= viewModel

        // Specify the current activity as the lifecycle owner of the binding. This is used so that
        // the binding can observe LiveData updates
        binding.setLifecycleOwner(this)

        viewModel.toast.observe(this, Observer { newToast: String ->
            Toast.makeText(this.requireContext(), newToast, Toast.LENGTH_LONG).show()
        })

//        viewModel.reportSubmitted.observe(this, Observer { newVal ->
//            if (newVal == true) {
//                binding.let {
//                    it.queryText.visibility = View.GONE
//                    it.signalSpinner.visibility = View.GONE
//                    it.submitRequestButton.visibility = View.GONE
//                    it.numAgentsButton.visibility = View.GONE
//                    it.numAgentsText.visibility = View.GONE
//                    it.lastBlockButton.visibility = View.GONE
//                    it.lastBlockText.visibility = View.GONE
//                    it.waitText.visibility = View.VISIBLE
//                }
//            }
//        })
//
//        viewModel.reportAccepted.observe(this, Observer { newVal ->
//            if (newVal == true) {
//                binding.let {
//                    it.waitText.visibility = View.GONE
//                    it.revealButton.visibility = View.VISIBLE
//                    it.revealText.visibility = View.VISIBLE
//                    it.scoreButton.visibility = View.VISIBLE
//                    it.scoreText.visibility = View.VISIBLE
//                    it.updateButton.visibility = View.VISIBLE
//                    it.updateText.visibility = View.VISIBLE
//                    it.getScoreButton.visibility = View.VISIBLE
//                    it.getScoreText.visibility = View.VISIBLE
//                    it.getBalanceButton.visibility = View.VISIBLE
//                    it.getBalanceText.visibility = View.VISIBLE
//                }
//            }
//        })

        viewModel.signals.observe(this, Observer { newSignals ->

            val signalSpinner: Spinner? = view?.findViewById(R.id.signal_spinner)
            val spinnerItems: Array<String> = newSignals
            val arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, spinnerItems)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            signalSpinner?.adapter = arrayAdapter

            signalSpinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    viewModel.setSignal(spinnerItems[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            }
        })

        return binding.root

    }

}
