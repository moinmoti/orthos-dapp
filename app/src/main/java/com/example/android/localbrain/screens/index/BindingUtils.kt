package com.example.android.localbrain.screens.index

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.android.localbrain.database.Query

@BindingAdapter("queryText")
fun TextView.setQueryText(item: Query) {
    item.let{
        text = item.text
    }
}