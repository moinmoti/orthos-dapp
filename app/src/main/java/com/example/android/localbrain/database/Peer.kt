package com.example.android.localbrain.database

data class Peer (
        val id: Long, val addr: String, val latitude: Double, val longitude: Double
)