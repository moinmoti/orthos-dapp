package com.example.android.localbrain.screens.index

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.localbrain.Globals
import com.example.android.localbrain.Orthos
import com.example.android.localbrain.database.Query
import org.web3j.crypto.Credentials
import org.web3j.protocol.Web3j
import org.web3j.protocol.infura.InfuraHttpService
import org.web3j.tx.gas.DefaultGasProvider
import java.math.BigInteger
import java8.util.concurrent.CompletableFuture
import java.util.concurrent.Future

class IndexViewModel: ViewModel() {
    companion object {
        // These represent different important times in the game, such as game length.
    }

    private val globals: Globals = Globals()
    private lateinit var web3: Web3j
    private val credentials: Credentials

    init {
        Log.d("IndexViewModel", "Private Key: ${globals.PRIVATE_KEY}")
        credentials = Credentials.create(globals.PRIVATE_KEY)
        initializeWeb3J()
    }

    private fun initializeWeb3J(): String {
        val result: String
        result = try {
            web3 = Web3j.build(InfuraHttpService(globals.INFURA_URL))
            "Success initializing web3j/infura"
        } catch (e: Exception) {
            val exception = e.toString()
            "Error initializing web3j/infura. Error: $exception"
        }

        return result
    }

    fun getQueries(): Array<Query> {
        var queries = emptyArray<Query>()
//        val numQueries = getNumQueries()
//        for (i in 0 until numQueries) {
//            var lb = LocalBrain.load(CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
//            val contractCall: Future<BigInteger>? = lb.queryBag(i.toBigInteger()).sendAsync()
//            val qid: Long = contractCall?.get()?.toLong() ?: -1
//            if (qid < 0) {
//                Log.d("IndexViewModel", "invalid Query Id")
//            } else if (qid.toInt() == 8 || qid.toInt() == 9 || qid.toInt() == 10) {
//                continue
//            } else {
//                lb = LocalBrain.load(CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
//                var contractCall2: Future<String>? = lb.getQuery(qid.toBigInteger()).sendAsync()
//                val query_text: String = contractCall2?.get() ?: "empty"
//                val query = Query(qid, query_text)
//                queries += query
//            }
//        }
        var orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: CompletableFuture<List<*>>? = orthos.allQueries.sendAsync()
        val qids = contractCall?.get() ?: emptyList<Any>()
        for (qid in qids) {
            orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
            var contractCall2: Future<String>? = orthos.getQuery(qid.toString().toBigInteger()).sendAsync()
            val query_text: String = contractCall2?.get() ?: "empty"
            val query = Query(qid.toString().toLong(), query_text)
            queries += query

        }
        Log.i("IndexViewModel", queries[0].text)
        return queries
    }

//    fun getNumQueries(): Int {
//        val lb = LocalBrain.load(CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
//        val contractCall: Future<BigInteger>? = lb.getNumQueries().sendAsync()
//        val callResult: BigInteger? = contractCall?.get()
//        return callResult?.toInt() ?: 0
//    }

    private val _navigateToQueryPage = MutableLiveData<Long>()
    val navigateToQueryPage
        get() = _navigateToQueryPage

    fun onQueryClicked(id: Long) {
        _navigateToQueryPage.value = id
    }

    fun onQueryPageNavigated() {
        _navigateToQueryPage.value = null
    }

}