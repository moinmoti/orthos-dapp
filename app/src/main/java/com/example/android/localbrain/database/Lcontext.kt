package com.example.android.localbrain.database

data class Lcontext (
        val id: Long, val text: String, val addr: String, val hash: ByteArray,
        val v: Int, val r: ByteArray, val s: ByteArray
    )