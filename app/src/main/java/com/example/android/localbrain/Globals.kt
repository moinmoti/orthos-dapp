package com.example.android.localbrain

import android.app.Application
import com.example.android.localbrain.database.Peer
import java.math.BigInteger

class Globals : Application() {

    companion object {
        val _CONTRACT_ADDRESS = "0xF93B1F5FbCc20E7447c93dc8D8452Cd580664bd8"
        val _INFURA_URL = "https://rinkeby.infura.io/3f8553440df94a8babe5960e8548a6f1"
        var _PRIVATE_KEY = ""
        val _dummyPeers = mutableListOf(
                "0x0C79E2CC0fF73e15Db53c50beA14797D60eBB357",
                "0x8bA57E9619A02BBCaDFfb4666d71189983697594",
                "0xf934324039aDa71A1451a65902C2637A1175EAda",
                "0x4Ee36339aeFc34Fa6397C0fE65C1Ab519F84d814",
                "0xdE115cA1C9e47341eF1BE5a83D24bE8678609D1a",
                "0xc9B14f93e8309f129322234fe03E19f7E0083481",
                "0x7D4C2DDd5F6c9f0c9F60cf0F9802C7A8EDe5E786"
        )
        val _dummyKeys = mutableListOf(
                "5afd7663b16e6d3fd7b7cf469fadadc0c6f2464d472bc15fe04fb070aee043bd",
                "378090E4CAE00A3B9DF2798CC88E1C39536AFE9F2500BDA26967C5181501DB73",
                "0A4810FC140B67673C0EE2AD20E18A246073919F42E1D0D1153E2E60960A66EC",
                "9C50D244F9D0F7A092CE39419EDC6A7DC9CDBBE03F277FA403451272FBA2A0DD",
                "79FD3756BEFD28D54A83410B59010EBC2E7FEC7D2F090171819F03738AD92112",
                "076E2446AA06933B936A65EAB716793F3BA7B1F7B3A5D7CB2A08CA2AE1CE9F85",
                "EEA380B3F8621CE3A95263074CDA87CA33A8569AAA7C1F96A4EFCCFDED150811"
        )
        val _otherpList = mutableListOf(
                "0x2F62f5f1c0A0a7EFaE7A2b02F9Bd6044104cCde0",
                "0x75C9Ee647F6CFDd27605B1451930CE89786ef0D0"
        )
        val _pList = mutableListOf<String>()
        val _hList = mutableListOf<ByteArray>()
        val _vList = mutableListOf<BigInteger>()
        val _rList = mutableListOf<ByteArray>()
        val _sList = mutableListOf<ByteArray>()
        val _peers = mutableListOf<Peer>()
    }

    val CONTRACT_ADDRESS get() = _CONTRACT_ADDRESS
    val INFURA_URL get() = _INFURA_URL
    val PRIVATE_KEY = _PRIVATE_KEY
    val dummyPeers get() = _dummyPeers
    val dummyKeys get() = _dummyKeys
    val otherpList get() = _otherpList
    val pList get() = _pList
    val hList get() = _hList
    val vList get() = _vList
    val rList get() = _rList
    val sList get() = _sList
    val peers get() = _peers

    fun setPKey(pKey: String) {
        _PRIVATE_KEY = pKey
    }

    fun addPAddr(p: String) {
        _pList += p
    }

    fun addH(h: ByteArray) {
        _hList += h
    }

    fun addV(v: BigInteger) {
        _vList += v
    }

    fun addR(r: ByteArray) {
        _rList += r
    }

    fun addS(s: ByteArray) {
        _sList += s
    }

    fun addPeer(p: Peer) {
        _peers += p
    }
}