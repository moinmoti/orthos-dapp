package com.example.android.localbrain.screens.index


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.localbrain.R
import com.example.android.localbrain.database.Query
import com.example.android.localbrain.databinding.IndexFragmentBinding

class IndexFragment : Fragment() {

    private lateinit var viewModel: IndexViewModel

    private lateinit var binding: IndexFragmentBinding

//    private lateinit var recyclerView: RecyclerView
//    private lateinit var viewAdapter: RecyclerView.Adapter<*>
//    private lateinit var viewManager: RecyclerView.LayoutManager


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.index_fragment,
                container,
                false
        )

        // Get the viewmodel
        viewModel = ViewModelProviders.of(this).get(IndexViewModel::class.java)

        // Set the viewmodel for databinding - this allows the bound layout access to all of the
        // data in the VieWModel
        binding.indexViewModel = viewModel

        // Specify the current activity as the lifecycle owner of the binding. This is used so that
        // the binding can observe LiveData updates
        binding.setLifecycleOwner(this)

        val myDataset: Array<Query> = viewModel.getQueries()

        val adapter = IndexAdapter(myDataset, QueryListener {
            queryId -> viewModel.onQueryClicked(queryId)
        })
        binding.indexViewList.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        binding.indexViewList.adapter = adapter

        viewModel.navigateToQueryPage.observe(this, Observer {queryId ->
            queryId?.let {
                Log.d("IndexFragment", "Query Clicked")
                this.findNavController().navigate(IndexFragmentDirections.actionIndexToQuery(queryId))
            }
//            viewModel.onQueryPageNavigated()
        })

        return binding.root

    }


}
