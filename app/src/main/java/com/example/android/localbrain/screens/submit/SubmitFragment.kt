package com.example.android.localbrain.screens.submit


import android.opengl.Visibility
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.android.localbrain.R
import com.example.android.localbrain.databinding.SubmitFragmentBinding

/**
 * A simple [Fragment] subclass.
 *
 */
class SubmitFragment : Fragment() {

    private lateinit var viewModel: SubmitViewModel
    private lateinit var binding: SubmitFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.submit_fragment,
                container,
                false
        )
        viewModel = ViewModelProviders.of(this).get(SubmitViewModel::class.java)
        binding.submitViewModel = viewModel
        binding.setLifecycleOwner(this)

        binding.submitRequestButton.setOnClickListener {
            viewModel.addQuery(
                    binding.queryTextEdit.text.toString(),
                    binding.allSignalEdit.text.toString(),
                    binding.payEdit.text.toString().toDouble()
            )
        }

//        binding.addSignalButton.setOnClickListener {
////            viewModel.addSignal(binding.signalEdit.text.toString())
//            binding.signalEdit.text = null
//        }

        viewModel.toast.observe(this, Observer { newToast: String ->
            Toast.makeText(this.requireContext(), newToast, Toast.LENGTH_LONG).show()
        })

//        viewModel.signalFormVisibility.observe(this, Observer { visibility ->
//            binding.addSignalButton.visibility = if (visibility) View.VISIBLE else View.INVISIBLE
//            binding.signalEdit.visibility = if (visibility) View.VISIBLE else View.INVISIBLE
//        })

        return binding.root
    }


}
