package com.example.android.localbrain.screens.query

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class QueryViewModelFactory (
        private val queryId: Long
    ): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(QueryViewModel::class.java)) {
            return QueryViewModel(queryId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}