package com.example.android.localbrain.screens.advertise

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.localbrain.database.Lcontext
import com.example.android.localbrain.databinding.ListItemLcontextBinding

class AdvertiseAdapter(val clickListener: LcontextListener): ListAdapter<Lcontext, AdvertiseAdapter.ViewHolder>(LcontextDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        return ViewHolder.from(parent)
    }

//    override fun getItemCount(): Int = myDataset.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(clickListener, item)
    }

    class ViewHolder private constructor (val binding: ListItemLcontextBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(clickListener: LcontextListener, item: Lcontext) {
            binding.lcontext = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemLcontextBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class LcontextDiffCallback: DiffUtil.ItemCallback<Lcontext>() {
    override fun areItemsTheSame(oldItem: Lcontext, newItem: Lcontext): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Lcontext, newItem: Lcontext): Boolean {
        return oldItem == newItem
    }

}

class LcontextListener(val clickListener: (queryId: Long) -> Unit) {
    fun onClick(lcontext: Lcontext) = clickListener(lcontext.id)
}
