package com.example.android.localbrain.screens.password


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.android.localbrain.Globals
import com.example.android.localbrain.R
import com.example.android.localbrain.databinding.PasswordFragmentBinding

/**
 * A simple [Fragment] subclass.
 */
class PasswordFragment : Fragment() {

    private val globals: Globals = Globals()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: PasswordFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.password_fragment, container, false)
        binding.submitPkeyButton.setOnClickListener {
            globals.setPKey(binding.pkeyEdit.text.toString())
            findNavController().navigate(PasswordFragmentDirections.actionPasswordToTitle())
        }
        return binding.root
    }


}
