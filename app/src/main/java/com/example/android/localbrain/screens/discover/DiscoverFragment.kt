package com.example.android.localbrain.screens.discover


import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.android.localbrain.R
import com.example.android.localbrain.databinding.DiscoverFragmentBinding
import com.google.android.gms.nearby.Nearby
import android.content.DialogInterface
import android.location.Location
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.example.android.localbrain.Globals
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.nearby.connection.*
import kotlinx.coroutines.*
import org.json.JSONObject
import org.web3j.crypto.Credentials
import org.web3j.crypto.Sign


class DiscoverFragment : Fragment() {

    private val globals: Globals = Globals()

    private lateinit var viewModel: DiscoverViewModel
    private lateinit var binding: DiscoverFragmentBinding

    private lateinit var connectionsClient: ConnectionsClient
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val strategy = Strategy.P2P_STAR
    private val serviceId = "com.example.android.localbrain"
    private val nickname = "LocalBrain"

    private var peersReceived = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.discover_fragment,
                container,
                false
        )

        viewModel = ViewModelProviders.of(this).get(DiscoverViewModel::class.java)
        binding.discoverViewModel = viewModel

        binding.lifecycleOwner = this

        viewModel.startDiscoveryEvent.observe(this, Observer { newVal ->
            if (newVal == true) {
                startDiscovery()
                viewModel.discoveryStarted()
            }
        })

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        connectionsClient = Nearby.getConnectionsClient(requireContext())
    }

    override fun onStop() {
        super.onStop()
        connectionsClient.stopDiscovery()
        connectionsClient.stopAllEndpoints()
    }

    private fun startDiscovery() {
        val discoveryOptions = DiscoveryOptions.Builder().setStrategy(strategy).build()
        connectionsClient.startDiscovery(serviceId, endpointDiscoveryCallback, discoveryOptions)
                .addOnSuccessListener {
                    Log.d("DiscoverFragment", "Discovering...")
                }
                .addOnFailureListener { e: Exception ->
                    Log.d("DiscoverFragment", e.toString())
                }
    }

    private val endpointDiscoveryCallback = object : EndpointDiscoveryCallback() {
        override fun onEndpointFound(endpointId: String, info: DiscoveredEndpointInfo) {
            // An endpoint was found. We request a connection to it.
            connectionsClient.requestConnection(nickname, endpointId, connectionLifecycleCallback)
                    .addOnSuccessListener {
                        Log.d(
                            "DiscoverFragment",
                            "We successfully requested a connection. " +
                                    "Now both sides must accept before the connection is established"
                        )
                    }
                    .addOnFailureListener { e: Exception ->
                        Log.d("DiscoverFragment", "Nearby Connections failed to request the connection")
                    }
        }

        override fun onEndpointLost(endpointId: String) {
            Log.d("DiscoverFragment", "A previously discovered endpoint has gone away")
        }
    }

    private val connectionLifecycleCallback = object : ConnectionLifecycleCallback() {

        override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
            // Automatically accept the connection on both sides.
//            connectionsClient.acceptConnection(endpointId, payloadCallback)
            AlertDialog.Builder(requireContext())
                    .setTitle("Accept connection to " + connectionInfo.endpointName)
                    .setMessage("Confirm the code matches on both devices: " + connectionInfo.authenticationToken)
                    .setPositiveButton(
                            "Accept"
                    ) { dialog: DialogInterface, which: Int ->
                        Log.d("DiscoverFragment", "The user confirmed, so we can accept the connection")
                        connectionsClient.acceptConnection(endpointId, payloadCallback)
                    }
                    .setNegativeButton(
                            android.R.string.cancel
                    ) { dialog: DialogInterface, which: Int ->
                        Log.d("DiscoverFragment", "The user canceled, so we should reject the connection")
                        connectionsClient.rejectConnection(endpointId)
                    }
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
        }

        @SuppressLint("MissingPermission")
        override fun onConnectionResult(endpointId: String, result: ConnectionResolution) {
            when (result.status.statusCode) {
                ConnectionsStatusCodes.STATUS_OK -> runBlocking {
                    Log.d("DiscoverFragment", "We're connected! Can now start sending and receiving data")

                    fusedLocationClient.lastLocation
                            .addOnSuccessListener { location : Location? ->
                                if (location != null) {
                                    val contextObj = JSONObject()
                                    val addr: String = Credentials.create(globals.PRIVATE_KEY).address
                                    contextObj.put("type", "peer")
                                    contextObj.put("address", addr)
                                    contextObj.put("latitude", location.latitude)
                                    contextObj.put("longitude", location.longitude)
                                    connectionsClient.sendPayload(
                                            endpointId,
                                            Payload.fromBytes(contextObj.toString().toByteArray())
                                    )
                                }
                            }

                    val job = launch (Dispatchers.Default) {
                        while (isActive) {
                            if (peersReceived) {
                                val lcObj = JSONObject()
                                val sig = Sign.signPrefixedMessage("VALIDATED".toByteArray(),
                                        Credentials.create(globals.PRIVATE_KEY).ecKeyPair)
                                lcObj.put("type", "signature")
                                lcObj.put("address", Credentials.create(globals.PRIVATE_KEY).address)
                                lcObj.put("hash", "VALIDATED".toByteArray())
                                lcObj.put("v", sig.v.toInt())
                                lcObj.put("r", sig.r.toString())
                                lcObj.put("s", sig.s.toString())
                                val lcString = lcObj.toString()
                                connectionsClient.sendPayload(
                                        endpointId,
                                        Payload.fromBytes(lcString.toByteArray())
                                )
                            }
                        }
                    }
                    delay(15000L)
                    job.cancelAndJoin()
                    Toast.makeText(context, "Ending Connection", Toast.LENGTH_LONG).show()
                }
                ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> {
                    Log.d("DiscoverFragment", "The connection was rejected by one or both sides")
                }
                ConnectionsStatusCodes.STATUS_ERROR -> {
                    Log.d("DiscoverFragment", "The connection broke before it was able to be accepted")
                }
                else -> {
                    Log.d("DiscoverFragment", "Unknown status code")
                }
            }
        }

        override fun onDisconnected(endpointId: String) {
            Log.d(
                    "DiscoverFragment",
                    "We've been disconnected from this endpoint. No more data can be sent or received"
            )
        }
    }

    private val payloadCallback = object : PayloadCallback() {
        override fun onPayloadReceived(endpointId: String, payload: Payload) {
            val objStr = payload.asBytes()?.toString()
            val jsonObj = JSONObject(objStr)
            if (jsonObj.getString("type") == "peerList") {
                peersReceived = true
            }
            Toast.makeText(context, "Peers Received", Toast.LENGTH_LONG).show()
        }

        override fun onPayloadTransferUpdate(endpointId: String, update: PayloadTransferUpdate) {
            if (update.status == PayloadTransferUpdate.Status.SUCCESS) {
                Log.d("DiscoverFragment", "Success Update Received")
            }
        }
    }
}
