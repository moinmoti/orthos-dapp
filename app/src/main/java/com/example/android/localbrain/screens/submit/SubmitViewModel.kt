package com.example.android.localbrain.screens.submit

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.localbrain.Globals
import com.example.android.localbrain.Orthos
import org.web3j.crypto.Credentials
import org.web3j.protocol.Web3j
import org.web3j.protocol.core.methods.response.TransactionReceipt
import org.web3j.protocol.infura.InfuraHttpService
import org.web3j.tx.gas.DefaultGasProvider
import java.math.BigInteger
import java.util.concurrent.Future
import kotlin.math.pow

class SubmitViewModel: ViewModel() {
    companion object {
        // These represent different important times in the game, such as game length.
    }

    private val globals: Globals = Globals()

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast

    private lateinit var web3: Web3j
    private val credentials: Credentials

    private var queryId: Long = -1

    init {
        credentials = Credentials.create(globals.PRIVATE_KEY)
        initializeWeb3J()
    }

    private fun initializeWeb3J(): String {
        val result: String
        result = try {
            web3 = Web3j.build(InfuraHttpService(globals.INFURA_URL))
            "Success initializing web3j/infura"
        } catch (e: Exception) {
            val exception = e.toString()
            "Error initializing web3j/infura. Error: $exception"
        }

        return result
    }

    fun addQuery(queryText: String, allSignalsStr: String, payment: Double) {
        val newSignalsStr = "Blank;" + allSignalsStr
        val allSignals = newSignalsStr.split(";")
        Log.d("Submit View Model", allSignalsStr)
        Log.d("Submit View Model", allSignals[0])
        println(allSignals)
        var result: String
        try {
            val orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
            val transactionReceipt: Future<TransactionReceipt>? = orthos.addQuery(
                queryText, allSignals, "rptsc",
                payment.pow(18).toLong().toBigInteger()
            ).sendAsync()
            result = "Successful transaction. Gas used: ${transactionReceipt?.get()?.gasUsed}"
            getQueryId(queryText)
        } catch (e: Exception) {
            result = "Error during transaction. Error: " + e.message
        }
        _toast.value = result
    }

    private fun getQueryId(queryText: String) {
        val orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: Future<BigInteger>? = orthos.getQueryId(queryText).sendAsync()
        queryId = contractCall?.get()?.toLong() ?: -1
        Log.d("SubmitQueryModel", "Query Id: $queryId")
        _toast.value = "Query Id: $queryId"
    }

//    fun addSignal(signal: String) {
//        val result = try {
//            val lb = LocalBrain.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
//            val transactionReceipt: Future<TransactionReceipt>? = lb.addSignal(
//                    queryId.toBigInteger(), signal
//            ).sendAsync()
//            "Successful transaction. Gas used: ${transactionReceipt?.get()?.gasUsed}"
//        } catch (e: Exception) {
//            "Error during transaction. Error: " + e.message
//        }
//        _toast.value = result
//    }
}