package com.example.android.localbrain.database

data class Query (val id: Long, val text: String) {
    var numSignals: Int = 0
    var signals = emptyArray<String>()
    var numAgents: Int = 0
}