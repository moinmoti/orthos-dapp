/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.localbrain.screens.query

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.android.localbrain.Globals
import com.example.android.localbrain.Orthos
//import org.web3j.abi.FunctionEncoder
//import org.web3j.abi.datatypes.Function
//import org.web3j.abi.datatypes.Type
//import org.web3j.abi.datatypes.Uint
//import org.web3j.abi.datatypes.Utf8String
//import org.web3j.abi.datatypes.generated.Bytes32
import org.web3j.crypto.Credentials
import org.web3j.crypto.Hash.sha3
//import org.web3j.crypto.RawTransaction
//import org.web3j.crypto.TransactionEncoder
import org.web3j.protocol.Web3j
//import org.web3j.protocol.core.DefaultBlockParameter
//import org.web3j.protocol.core.DefaultBlockParameterName
//import org.web3j.protocol.core.methods.request.Transaction
//import org.web3j.protocol.core.methods.response.EthGetTransactionCount
import org.web3j.protocol.core.methods.response.TransactionReceipt
import org.web3j.protocol.infura.InfuraHttpService
import org.web3j.tx.gas.DefaultGasProvider
import java.math.BigInteger
import java8.util.concurrent.CompletableFuture
import org.web3j.abi.datatypes.Address
import org.web3j.abi.datatypes.generated.Bytes32
import org.web3j.crypto.Sign
import org.web3j.tx.gas.ContractGasProvider
import java.util.concurrent.Future


/**
 * ViewModel containing all the logic needed to run the game
 */
class QueryViewModel (
        queryId: Long
    ) : ViewModel() {


    companion object {
        // These represent different important times in the game, such as game length.
    }

    private val globals: Globals = Globals()

    private val _toast = MutableLiveData<String>()
    val toast: LiveData<String>
        get() = _toast

    private val _gasUsage = MutableLiveData<String>()
    val gasUsage: LiveData<String>
        get() = _gasUsage

    private var totalGas: Long = 0

    private val _revealGas = MutableLiveData<String>()
    val revealGas: LiveData<String>
        get() = _revealGas

    private val _calcGas = MutableLiveData<String>()
    val calcGas: LiveData<String>
        get() = _calcGas

    private val _updateGas = MutableLiveData<String>()
    val updateGas: LiveData<String>
        get() = _updateGas

    private val _newScore = MutableLiveData<String>()
    val newScore: LiveData<String>
        get() = _newScore

    private val _newBalance = MutableLiveData<String>()
    val newBalance: LiveData<String>
        get() = _newBalance

    private val _reportSubmitted = MutableLiveData<Boolean>()
    val reportSubmitted: LiveData<Boolean>
        get() = _reportSubmitted

    private val _reportAccepted = MutableLiveData<Boolean>()
    val reportAccepted: LiveData<Boolean>
        get() = _reportAccepted


    private lateinit var web3: Web3j
    private val credentials: Credentials

    private var report: String
    private var secret: String
    private lateinit var finalReport: ByteArray

//    private var newSignalSpace = emptyArray<String>()

    private var qid: Long
    private var numSignals: Int = 0
    private val _signals = MutableLiveData<Array<String>>()
    val signals: LiveData<Array<String>>
        get() = _signals

    private val _query = MutableLiveData<String>()
    val query: LiveData<String>
        get() = _query

    private val _blockNumber = MutableLiveData<Int>()
    val blockNumber: LiveData<Int>
        get() = _blockNumber
    val blockNumberString = Transformations.map(blockNumber) { num ->
        num.toString()
    }

    private val _numAgents = MutableLiveData<Int>()
    val numAgents: LiveData<Int>
        get() = _numAgents
    val numAgentsString = Transformations.map(numAgents) { num ->
        num.toString()
    }

    private val dummyPList = globals.dummyPeers
    private val dummyHList = mutableListOf<ByteArray>()
    private val dummyVList = mutableListOf<BigInteger>()
    private val dummyRList = mutableListOf<ByteArray>()
    private val dummySList = mutableListOf<ByteArray>()


    init {
//        web3 = Web3j.build(InfuraHttpService(INFURA_URL))
        _reportSubmitted.value = false
        _reportAccepted.value = false

        qid = queryId
        report = ""
        secret = "secret"
        credentials = Credentials.create(globals.PRIVATE_KEY)

        initializeWeb3J()
        getQuery()
        getNumSignals()
        Log.i("QueryViewModel", "numSignals: $numSignals")
        getSignals()
        Log.i("QueryViewModel", "Signal Space Size: ${_signals.value?.size}")
        makeDummySigData()
    }

    private fun makeDummySigData() {
        val hash = sha3("VALIDATED".toByteArray())
        for (k in globals.dummyKeys) {
            val c = Credentials.create(k)
            val sig = Sign.signPrefixedMessage(hash, c.ecKeyPair)
            dummyHList += hash
            dummyVList += sig.v.toInt().toBigInteger()
            dummyRList += sig.r
            dummySList += sig.s
        }
    }

    private fun getQuery() {
        val orthos: Orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: Future<String>? = orthos.getQuery(qid.toBigInteger()).sendAsync()
        val callResult: String? = contractCall?.get()

        if (callResult != null) {
            _query.value = callResult
        } else {
            _query.value = "Query"
        }
    }

    private fun getNumSignals() {
        val orthos: Orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: Future<BigInteger>? = orthos.getNumSignals(qid.toBigInteger()).sendAsync()
        val callResult: BigInteger? = contractCall?.get()

        numSignals = callResult?.toInt() ?: 0
    }

    private fun getSignals() {
//        var newSignalSpace = emptyArray<String>()
        val orthos: Orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: CompletableFuture<List<*>>? = orthos.getAllSignals(qid.toBigInteger()).sendAsync()
        var allSignals = contractCall?.get()?.map{it as String}?.toTypedArray() ?: emptyArray()
        _signals.value = allSignals.drop(1).toTypedArray()
        Log.d("QueryViewModel", allSignals[0])
//        for (i in 0 until numSignals) {
//            val orthos: Orthos = Orthos.load(CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
//            val contractCall: Future<String>? = orthos.getSignal(qid.toBigInteger(), i.toBigInteger()).sendAsync()
//            val callResult: String = contractCall?.get() ?: ""
//            Log.i("QueryViewModel", "New Signal: $callResult")
//            newSignalSpace += callResult
//            Log.i("QueryViewModel", "New Signal Space: $newSignalSpace")
//            _signals.value = newSignalSpace
//        }
    }

    private fun initializeWeb3J(): String {
        val result: String
        result = try {
            web3 = Web3j.build(InfuraHttpService(globals.INFURA_URL))
            "Success initializing web3j/infura"
        } catch (e: Exception) {
            val exception = e.toString()
            "Error initializing web3j/infura. Error: $exception"
        }

        return result
    }

    fun setSignal(_report: String) {
        report = _report
        val tempString = report + secret
        finalReport = sha3(tempString.toByteArray())
//        _toast.value = report
    }

    fun onSubmit() {
        _reportSubmitted.value = true
        totalGas = 0
        val result: String
//        result = try {
            val orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
//            val transactionReceipt: Future<TransactionReceipt>? = orthos.submit(
//                    qid.toBigInteger(),
//                    finalReport,
//                    globals.pList,
//                    globals.hList,
//                    globals.vList,
//                    globals.rList,
//                    globals.sList
//            ).sendAsync()
            val transactionReceipt: Future<TransactionReceipt>? = orthos.submit(
                    qid.toBigInteger(),
                    finalReport,
                    dummyPList,
                    dummyHList,
                    dummyVList,
                    dummyRList,
                    dummySList
            ).sendAsync()
            _blockNumber.value = transactionReceipt?.get()?.blockNumber?.toInt()
            totalGas = transactionReceipt?.get()?.gasUsed!!.toLong()
            _gasUsage.value = totalGas.toString()
            result = "Successful transaction. Gas used: ${transactionReceipt?.get()?.gasUsed}"
//        } catch (e: Exception) {
//            "Error during transaction. Error: " + e.message
//        }
        _toast.value = result
        _reportAccepted.value = true

    }

    fun onReveal() {
        val result: String
        result = try {
            val orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
            val transactionReceipt: Future<TransactionReceipt>? = orthos.reveal(
                    qid.toBigInteger(),
                    report, secret
            ).sendAsync()
            _blockNumber.value = transactionReceipt?.get()?.blockNumber?.toInt()
            _revealGas.value = transactionReceipt?.get()?.gasUsed.toString()
            totalGas += transactionReceipt?.get()!!.gasUsed.toLong()
            _gasUsage.value = totalGas.toString()
            "Successful transaction. Gas used: ${transactionReceipt?.get()?.gasUsed}"
        } catch (e: Exception) {
            "Error during transaction. Error: " + e.message
        }
        _toast.value = result
    }

    fun onCalcScore() {
        val result: String
//        val GAS_PRICE = DefaultGasProvider().gasPrice
//        val GAS_LIMIT = 100000000.toBigInteger()
        result = try {
            val orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
            val transactionReceipt: Future<TransactionReceipt>? = orthos.calcScore(
                    qid.toBigInteger()).sendAsync()
            _blockNumber.value = transactionReceipt?.get()?.blockNumber?.toInt()
            _calcGas.value = transactionReceipt?.get()?.gasUsed.toString()
            "Successful transaction. Gas used: ${transactionReceipt?.get()?.gasUsed}"
        } catch (e: Exception) {
            "Error during transaction. Error: " + e.message
        }
        _toast.value = result
    }

    fun onUpdateBalance() {
        val result: String
        result = try {
            val orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
            val transactionReceipt: Future<TransactionReceipt>? = orthos.updateBalance(
                    qid.toBigInteger()).sendAsync()
            _blockNumber.value = transactionReceipt?.get()?.blockNumber?.toInt()
            _updateGas.value = transactionReceipt?.get()?.gasUsed.toString()
            "Successful transaction. Gas used: ${transactionReceipt?.get()?.gasUsed}"
        } catch (e: Exception) {
            "Error during transaction. Error: " + e.message
        }
        _toast.value = result
    }

    fun onGetBalance() {
        val orthos: Orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: CompletableFuture<BigInteger>? = orthos.balance.sendAsync()
        _newBalance.value = contractCall?.get()?.toString()
    }

    fun onGetScore() {
        val orthos: Orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: CompletableFuture<BigInteger>? = orthos.getScore(
                qid.toBigInteger()).sendAsync()
        _newScore.value = contractCall?.get()?.toString()
    }

    fun onNumAgents() {
        val orthos = Orthos.load(globals.CONTRACT_ADDRESS, web3, credentials, DefaultGasProvider())
        val contractCall: Future<BigInteger>? = orthos.getNumAgents(qid.toBigInteger()).sendAsync()
        val callResult = contractCall?.get()
        _numAgents.value = callResult?.toInt() ?: -1
    }

//    override fun onCleared() {
//        super.onCleared()
//    }
}


// RAW TRANSACTION SNIPPET
//            val function = Function(
//                    "submit",
//                    listOf(Utf8String("context"), Bytes32(finalReport)),
//                    emptyList()
//            )
//            val encodedFunction = FunctionEncoder.encode(function)
//            val rawTransaction = RawTransaction.createTransaction(
//                    getNonce(), DefaultGasProvider.GAS_PRICE,
//                    DefaultGasProvider.GAS_LIMIT, CONTRACT_ADDRESS, encodedFunction
//            )
//            val signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials)
//            val hexValue = Numeric.toHexString(signedMessage)
//            Log.d("QueryViewModel", "before sending raw transaction")
//            val ethSendTransaction= web3.ethSendRawTransaction(hexValue).sendAsync().get()
//            Log.d("QueryViewModel", "after sending raw transaction")
//            val transactionHash: String = ethSendTransaction.transactionHash
//            Log.d("QueryViewModel", "before receipt call")
//            val transactionReceipt = web3.ethGetTransactionReceipt(transactionHash).sendAsync().get()
//            Log.d("QueryViewModel", "after receipt call")
//            "Successful transaction. Gas used: ${transactionReceipt.transactionReceipt.get().gasUsed}"