pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

//import "./strings.sol";
//import "./double.sol";

/*
#    Copyright (C) 2017  alianse777

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

library Double {
    //uint private dscale = 2;  // precision
    //uint private dot = 10**dscale;

    struct double {
        uint part;
        uint f;
        bool sign;
        uint prec;
    }

    //function setPrecision(uint prec) internal {
        //dscale = prec;
        //dot = 10**dscale;
    //}

    /**
    * @dev Creates new double instanse a.b
    * @param integral fractional
    * @return double(integral.fractional)
    */
    function newDouble(int integral, uint fractional, uint precision)
    internal pure returns (double memory data) {
        if (integral < 0) {
            data.sign = true;
            data.part = uint(-integral);
        } else {
            data.part = uint(integral);
        }
        data.f = fractional;
        data.prec = precision;
    }

    function convert(double memory data)
    internal pure returns (uint r) {
        uint dot = 10 ** data.prec;
        uint256 integer = data.part*dot;
        assert(integer / data.part == dot);
        r = integer + data.f;
        assert(r - data.f == integer);
    }

    function normalize(uint num, uint exp, bool sign, uint precision)
    internal pure returns (double memory data) {
        uint d = 10**exp;
        uint part = num / d;
        uint f = num % d;
        uint dot = 10 ** precision;
        if (f % dot == 0) {
            f /= dot;
        }
        data.part = part;
        data.f = f;
        data.sign = sign;
        data.prec = precision;
    }

    // built-in ^ operator does not support bools.
    function xor(bool a, bool b)
    internal pure returns (bool) {
        if (a && b) return false;
        if (a || b) return true;
        return false;
    }

    /*
    // add, sub, mult, div
    */

    /**
    * @dev Sum two doubles
    * @param lhs rhs
    * @return lhs + rhs
    */
    function doubleAdd(double memory lhs, double memory rhs)
    internal pure returns (double memory) {
        uint l = convert(lhs);
        uint r = convert(rhs);
        uint precision = (lhs.prec < rhs.prec) ? lhs.prec : rhs.prec;
        if (lhs.sign == rhs.sign) {
            return normalize(l + r, 2, rhs.sign, precision);
        } else {
            if (l > r) {
                return normalize(l - r, 2, lhs.sign, precision);
            } else {
                return normalize(r - l, 2, rhs.sign, precision);
            }
        }
    }

    /**
     * @param lhs rhs
     * @return lhs - rhs
     */
    function doubleSub(double memory lhs, double memory rhs)
    internal pure returns (double memory) {
        uint l = convert(lhs);
        uint r = convert(rhs);
        uint precision = (lhs.prec < rhs.prec) ? lhs.prec : rhs.prec;
        if (l > r) {
            if (lhs.sign != rhs.sign) {
                return normalize(l + r, 2, lhs.sign, precision);
            } else {
                return normalize(l - r, 2, lhs.sign, precision);
            }
        } else if (l < r) {
            if (rhs.sign != lhs.sign) {
                return normalize(l + r, 2, !lhs.sign, precision);
            } else {
                return normalize(r - l, 2, !lhs.sign, precision);
            }
        } else {
            return newDouble(0, 0, precision);
        }
    }

    /**
     * @param lhs rhs
     * @return lhs * rhs
     */
    function doubleMult(double memory lhs, double memory rhs)
    internal pure returns (double memory) {
        uint l = convert(lhs);
        uint r = convert(rhs);
        uint precision = (lhs.prec < rhs.prec) ? lhs.prec : rhs.prec;
        return normalize(l*r, 4, xor(lhs.sign, rhs.sign), precision);
    }

    /**
     * @param lhs rhs
     * @return lhs / rhs
     */
    function doubleDiv(double memory lhs, double memory rhs)
    internal pure returns (double memory) {
        uint l = convert(lhs);
        uint r = convert(rhs);
        uint precision = (lhs.prec < rhs.prec) ? lhs.prec : rhs.prec;
        return normalize(l*(10**precision) / r, 2, xor(lhs.sign, rhs.sign), precision);
    }


    /*
    //  Equality
    */

    function doubleLt(double memory lhs, double memory rhs)
    internal pure returns (bool) {
        if (lhs.sign && !rhs.sign) {
            return true;
        } else if (!lhs.sign && rhs.sign) {
            return false;
        }
        if (lhs.part == rhs.part) {
            return lhs.f < rhs.f;
        }
        return lhs.part < rhs.part;
    }

    function doubleLe(double memory lhs, double memory rhs)
    internal pure returns (bool) {
        return (doubleLt(lhs, rhs) || doubleEq(lhs, rhs));
    }

    function doubleEq(double memory lhs, double memory rhs)
    internal pure returns (bool) {
        return (lhs.part == rhs.part && lhs.f == rhs.f && rhs.sign == lhs.sign);
    }

    function doubleGe(double memory lhs, double memory rhs)
    internal pure returns (bool) {
        return (doubleEq(lhs, rhs) || doubleGt(lhs, rhs));
    }

    function doubleGt(double memory lhs, double memory rhs)
    internal pure returns (bool) {
        if (lhs.sign && !rhs.sign) {
            return false;
        } else if (!lhs.sign && rhs.sign) {
            return true;
        }
        if (lhs.part == rhs.part) {
            return lhs.f > rhs.f;
        }
        return lhs.part > rhs.part;
    }

    /*
    // Conversation API
    */

    function doubleFromArray(int[] memory data)
    internal pure returns (double[] memory) {
        uint r_index = 0;
        double[] memory result = new double[](data.length / 2);
        for (uint i = 0; i < data.length-1; i += 2) {
            result[r_index].part = uint(data[i]);
            result[r_index].f = uint(data[i+1]);
            r_index++;
        }
        return result;
    }

    function doubleToArray(double[] memory data)
    internal pure returns (int[] memory) {
        uint index = 0;
        int[] memory result = new int[](data.length * 2);
        for (uint i = 0; i < data.length; i++) {
            if (data[i].sign)
                result[index] = -int(data[i].part);
            else
                result[index] = int(data[i].part);
            result[index+1] = int(data[i].f);
            index += 2;
        }
        return result;
    }

    function reshapeInt(int[2] memory data, uint precision)
    internal pure returns (double[] memory) {
        double[] memory res = new double[](data.length);
        for (uint i; i < data.length; i++) {
            res[i] = newDouble(data[i], 0, precision);
        }
        return res;
    }
}

/*
 * @title String & slice utility library for Solidity contracts.
 * @author Nick Johnson <arachnid@notdot.net>
 *
 */

library Strings {

    struct slice {
        uint _len;
        uint _ptr;
    }

    function memcpy(uint dest, uint src, uint len) private pure {
        // Copy word-length chunks while possible
        for(; len >= 32; len -= 32) {
            assembly {
                mstore(dest, mload(src))
            }
            dest += 32;
            src += 32;
        }

        // Copy remaining bytes
        uint mask = 256 ** (32 - len) - 1;
        assembly {
            let srcpart := and(mload(src), not(mask))
            let destpart := and(mload(dest), mask)
            mstore(dest, or(destpart, srcpart))
        }
    }

    /*
     * @dev Returns a slice containing the entire string.
     * @param self The string to make a slice from.
     * @return A newly allocated slice containing the entire string.
     */
    function toSlice(string memory self) internal pure returns (slice memory) {
        uint ptr;
        assembly {
            ptr := add(self, 0x20)
        }
        return slice(bytes(self).length, ptr);
    }

    /*
     * @dev Returns the length of a null-terminated bytes32 string.
     * @param self The value to find the length of.
     * @return The length of the string, from 0 to 32.
     */
    function len(bytes32 self) internal pure returns (uint) {
        uint ret;
        if (self == 0)
            return 0;
        if (uint(self) & 0xffffffffffffffffffffffffffffffff == 0) {
            ret += 16;
            self = bytes32(uint(self) / 0x100000000000000000000000000000000);
        }
        if (uint(self) & 0xffffffffffffffff == 0) {
            ret += 8;
            self = bytes32(uint(self) / 0x10000000000000000);
        }
        if (uint(self) & 0xffffffff == 0) {
            ret += 4;
            self = bytes32(uint(self) / 0x100000000);
        }
        if (uint(self) & 0xffff == 0) {
            ret += 2;
            self = bytes32(uint(self) / 0x10000);
        }
        if (uint(self) & 0xff == 0) {
            ret += 1;
        }
        return 32 - ret;
    }

    /*
     * @dev Returns a slice containing the entire bytes32, interpreted as a
     *      null-terminated utf-8 string.
     * @param self The bytes32 value to convert to a slice.
     * @return A new slice containing the value of the input argument up to the
     *         first null.
     */
    function toSliceB32(bytes32 self) internal pure returns (slice memory ret) {
        // Allocate space for `self` in memory, copy it there, and point ret at it
        assembly {
            let ptr := mload(0x40)
            mstore(0x40, add(ptr, 0x20))
            mstore(ptr, self)
            mstore(add(ret, 0x20), ptr)
        }
        ret._len = len(self);
    }

    /*
     * @dev Returns a new slice containing the same data as the current slice.
     * @param self The slice to copy.
     * @return A new slice containing the same data as `self`.
     */
    function copy(slice memory self) internal pure returns (slice memory) {
        return slice(self._len, self._ptr);
    }

    /*
     * @dev Copies a slice to a new string.
     * @param self The slice to copy.
     * @return A newly allocated string containing the slice's text.
     */
    function toString(slice memory self) internal pure returns (string memory) {
        string memory ret = new string(self._len);
        uint retptr;
        assembly { retptr := add(ret, 32) }

        memcpy(retptr, self._ptr, self._len);
        return ret;
    }

    /*
     * @dev Returns the length in runes of the slice. Note that this operation
     *      takes time proportional to the length of the slice; avoid using it
     *      in loops, and call `slice.empty()` if you only need to know whether
     *      the slice is empty or not.
     * @param self The slice to operate on.
     * @return The length of the slice in runes.
     */
    function len(slice memory self) internal pure returns (uint l) {
        // Starting at ptr-31 means the LSB will be the byte we care about
        uint ptr = self._ptr - 31;
        uint end = ptr + self._len;
        for (l = 0; ptr < end; l++) {
            uint8 b;
            assembly { b := and(mload(ptr), 0xFF) }
            if (b < 0x80) {
                ptr += 1;
            } else if(b < 0xE0) {
                ptr += 2;
            } else if(b < 0xF0) {
                ptr += 3;
            } else if(b < 0xF8) {
                ptr += 4;
            } else if(b < 0xFC) {
                ptr += 5;
            } else {
                ptr += 6;
            }
        }
    }

    /*
     * @dev Returns true if the slice is empty (has a length of 0).
     * @param self The slice to operate on.
     * @return True if the slice is empty, False otherwise.
     */
    function empty(slice memory self) internal pure returns (bool) {
        return self._len == 0;
    }

    /*
     * @dev Returns a positive number if `other` comes lexicographically after
     *      `self`, a negative number if it comes before, or zero if the
     *      contents of the two slices are equal. Comparison is done per-rune,
     *      on unicode codepoints.
     * @param self The first slice to compare.
     * @param other The second slice to compare.
     * @return The result of the comparison.
     */
    function compare(slice memory self, slice memory other) internal pure returns (int) {
        uint shortest = self._len;
        if (other._len < self._len)
            shortest = other._len;

        uint selfptr = self._ptr;
        uint otherptr = other._ptr;
        for (uint idx = 0; idx < shortest; idx += 32) {
            uint a;
            uint b;
            assembly {
                a := mload(selfptr)
                b := mload(otherptr)
            }
            if (a != b) {
                // Mask out irrelevant bytes and check again
                uint256 mask = uint256(-1); // 0xffff...
                if(shortest < 32) {
                  mask = ~(2 ** (8 * (32 - shortest + idx)) - 1);
                }
                uint256 diff = (a & mask) - (b & mask);
                if (diff != 0)
                    return int(diff);
            }
            selfptr += 32;
            otherptr += 32;
        }
        return int(self._len) - int(other._len);
    }

    /*
     * @dev Returns true if the two slices contain the same text.
     * @param self The first slice to compare.
     * @param self The second slice to compare.
     * @return True if the slices are equal, false otherwise.
     */
    function equals(slice memory self, slice memory other) internal pure returns (bool) {
        return compare(self, other) == 0;
    }

    /*
     * @dev Extracts the first rune in the slice into `rune`, advancing the
     *      slice to point to the next rune and returning `self`.
     * @param self The slice to operate on.
     * @param rune The slice that will contain the first rune.
     * @return `rune`.
     */
    function nextRune(slice memory self, slice memory rune) internal pure returns (slice memory) {
        rune._ptr = self._ptr;

        if (self._len == 0) {
            rune._len = 0;
            return rune;
        }

        uint l;
        uint b;
        // Load the first byte of the rune into the LSBs of b
        assembly { b := and(mload(sub(mload(add(self, 32)), 31)), 0xFF) }
        if (b < 0x80) {
            l = 1;
        } else if(b < 0xE0) {
            l = 2;
        } else if(b < 0xF0) {
            l = 3;
        } else {
            l = 4;
        }

        // Check for truncated codepoints
        if (l > self._len) {
            rune._len = self._len;
            self._ptr += self._len;
            self._len = 0;
            return rune;
        }

        self._ptr += l;
        self._len -= l;
        rune._len = l;
        return rune;
    }

    /*
     * @dev Returns the first rune in the slice, advancing the slice to point
     *      to the next rune.
     * @param self The slice to operate on.
     * @return A slice containing only the first rune from `self`.
     */
    function nextRune(slice memory self) internal pure returns (slice memory ret) {
        nextRune(self, ret);
    }

    /*
     * @dev Returns the number of the first codepoint in the slice.
     * @param self The slice to operate on.
     * @return The number of the first codepoint in the slice.
     */
    function ord(slice memory self) internal pure returns (uint ret) {
        if (self._len == 0) {
            return 0;
        }

        uint word;
        uint length;
        uint divisor = 2 ** 248;

        // Load the rune into the MSBs of b
        assembly { word:= mload(mload(add(self, 32))) }
        uint b = word / divisor;
        if (b < 0x80) {
            ret = b;
            length = 1;
        } else if(b < 0xE0) {
            ret = b & 0x1F;
            length = 2;
        } else if(b < 0xF0) {
            ret = b & 0x0F;
            length = 3;
        } else {
            ret = b & 0x07;
            length = 4;
        }

        // Check for truncated codepoints
        if (length > self._len) {
            return 0;
        }

        for (uint i = 1; i < length; i++) {
            divisor = divisor / 256;
            b = (word / divisor) & 0xFF;
            if (b & 0xC0 != 0x80) {
                // Invalid UTF-8 sequence
                return 0;
            }
            ret = (ret * 64) | (b & 0x3F);
        }

        return ret;
    }

    /*
     * @dev Returns the keccak-256 hash of the slice.
     * @param self The slice to hash.
     * @return The hash of the slice.
     */
    function keccak(slice memory self) internal pure returns (bytes32 ret) {
        assembly {
            ret := keccak256(mload(add(self, 32)), mload(self))
        }
    }

    /*
     * @dev Returns true if `self` starts with `needle`.
     * @param self The slice to operate on.
     * @param needle The slice to search for.
     * @return True if the slice starts with the provided text, false otherwise.
     */
    function startsWith(slice memory self, slice memory needle) internal pure returns (bool) {
        if (self._len < needle._len) {
            return false;
        }

        if (self._ptr == needle._ptr) {
            return true;
        }

        bool equal;
        assembly {
            let length := mload(needle)
            let selfptr := mload(add(self, 0x20))
            let needleptr := mload(add(needle, 0x20))
            equal := eq(keccak256(selfptr, length), keccak256(needleptr, length))
        }
        return equal;
    }

    /*
     * @dev If `self` starts with `needle`, `needle` is removed from the
     *      beginning of `self`. Otherwise, `self` is unmodified.
     * @param self The slice to operate on.
     * @param needle The slice to search for.
     * @return `self`
     */
    function beyond(slice memory self, slice memory needle) internal pure returns (slice memory) {
        if (self._len < needle._len) {
            return self;
        }

        bool equal = true;
        if (self._ptr != needle._ptr) {
            assembly {
                let length := mload(needle)
                let selfptr := mload(add(self, 0x20))
                let needleptr := mload(add(needle, 0x20))
                equal := eq(keccak256(selfptr, length), keccak256(needleptr, length))
            }
        }

        if (equal) {
            self._len -= needle._len;
            self._ptr += needle._len;
        }

        return self;
    }

    /*
     * @dev Returns true if the slice ends with `needle`.
     * @param self The slice to operate on.
     * @param needle The slice to search for.
     * @return True if the slice starts with the provided text, false otherwise.
     */
    function endsWith(slice memory self, slice memory needle) internal pure returns (bool) {
        if (self._len < needle._len) {
            return false;
        }

        uint selfptr = self._ptr + self._len - needle._len;

        if (selfptr == needle._ptr) {
            return true;
        }

        bool equal;
        assembly {
            let length := mload(needle)
            let needleptr := mload(add(needle, 0x20))
            equal := eq(keccak256(selfptr, length), keccak256(needleptr, length))
        }

        return equal;
    }

    /*
     * @dev If `self` ends with `needle`, `needle` is removed from the
     *      end of `self`. Otherwise, `self` is unmodified.
     * @param self The slice to operate on.
     * @param needle The slice to search for.
     * @return `self`
     */
    function until(slice memory self, slice memory needle) internal pure returns (slice memory) {
        if (self._len < needle._len) {
            return self;
        }

        uint selfptr = self._ptr + self._len - needle._len;
        bool equal = true;
        if (selfptr != needle._ptr) {
            assembly {
                let length := mload(needle)
                let needleptr := mload(add(needle, 0x20))
                equal := eq(keccak256(selfptr, length), keccak256(needleptr, length))
            }
        }

        if (equal) {
            self._len -= needle._len;
        }

        return self;
    }

    // Returns the memory address of the first byte of the first occurrence of
    // `needle` in `self`, or the first byte after `self` if not found.
    function findPtr(uint selflen, uint selfptr, uint needlelen, uint needleptr) private pure returns (uint) {
        uint ptr = selfptr;
        uint idx;

        if (needlelen <= selflen) {
            if (needlelen <= 32) {
                bytes32 mask = bytes32(~(2 ** (8 * (32 - needlelen)) - 1));

                bytes32 needledata;
                assembly { needledata := and(mload(needleptr), mask) }

                uint end = selfptr + selflen - needlelen;
                bytes32 ptrdata;
                assembly { ptrdata := and(mload(ptr), mask) }

                while (ptrdata != needledata) {
                    if (ptr >= end)
                        return selfptr + selflen;
                    ptr++;
                    assembly { ptrdata := and(mload(ptr), mask) }
                }
                return ptr;
            } else {
                // For long needles, use hashing
                bytes32 hash;
                assembly { hash := keccak256(needleptr, needlelen) }

                for (idx = 0; idx <= selflen - needlelen; idx++) {
                    bytes32 testHash;
                    assembly { testHash := keccak256(ptr, needlelen) }
                    if (hash == testHash)
                        return ptr;
                    ptr += 1;
                }
            }
        }
        return selfptr + selflen;
    }

    // Returns the memory address of the first byte after the last occurrence of
    // `needle` in `self`, or the address of `self` if not found.
    function rfindPtr(uint selflen, uint selfptr, uint needlelen, uint needleptr) private pure returns (uint) {
        uint ptr;

        if (needlelen <= selflen) {
            if (needlelen <= 32) {
                bytes32 mask = bytes32(~(2 ** (8 * (32 - needlelen)) - 1));

                bytes32 needledata;
                assembly { needledata := and(mload(needleptr), mask) }

                ptr = selfptr + selflen - needlelen;
                bytes32 ptrdata;
                assembly { ptrdata := and(mload(ptr), mask) }

                while (ptrdata != needledata) {
                    if (ptr <= selfptr)
                        return selfptr;
                    ptr--;
                    assembly { ptrdata := and(mload(ptr), mask) }
                }
                return ptr + needlelen;
            } else {
                // For long needles, use hashing
                bytes32 hash;
                assembly { hash := keccak256(needleptr, needlelen) }
                ptr = selfptr + (selflen - needlelen);
                while (ptr >= selfptr) {
                    bytes32 testHash;
                    assembly { testHash := keccak256(ptr, needlelen) }
                    if (hash == testHash)
                        return ptr + needlelen;
                    ptr -= 1;
                }
            }
        }
        return selfptr;
    }

    /*
     * @dev Modifies `self` to contain everything from the first occurrence of
     *      `needle` to the end of the slice. `self` is set to the empty slice
     *      if `needle` is not found.
     * @param self The slice to search and modify.
     * @param needle The text to search for.
     * @return `self`.
     */
    function find(slice memory self, slice memory needle) internal pure returns (slice memory) {
        uint ptr = findPtr(self._len, self._ptr, needle._len, needle._ptr);
        self._len -= ptr - self._ptr;
        self._ptr = ptr;
        return self;
    }

    /*
     * @dev Modifies `self` to contain the part of the string from the start of
     *      `self` to the end of the first occurrence of `needle`. If `needle`
     *      is not found, `self` is set to the empty slice.
     * @param self The slice to search and modify.
     * @param needle The text to search for.
     * @return `self`.
     */
    function rfind(slice memory self, slice memory needle) internal pure returns (slice memory) {
        uint ptr = rfindPtr(self._len, self._ptr, needle._len, needle._ptr);
        self._len = ptr - self._ptr;
        return self;
    }

    /*
     * @dev Splits the slice, setting `self` to everything after the first
     *      occurrence of `needle`, and `token` to everything before it. If
     *      `needle` does not occur in `self`, `self` is set to the empty slice,
     *      and `token` is set to the entirety of `self`.
     * @param self The slice to split.
     * @param needle The text to search for in `self`.
     * @param token An output parameter to which the first token is written.
     * @return `token`.
     */
    function split(slice memory self, slice memory needle, slice memory token) internal pure returns (slice memory) {
        uint ptr = findPtr(self._len, self._ptr, needle._len, needle._ptr);
        token._ptr = self._ptr;
        token._len = ptr - self._ptr;
        if (ptr == self._ptr + self._len) {
            // Not found
            self._len = 0;
        } else {
            self._len -= token._len + needle._len;
            self._ptr = ptr + needle._len;
        }
        return token;
    }

    /*
     * @dev Splits the slice, setting `self` to everything after the first
     *      occurrence of `needle`, and returning everything before it. If
     *      `needle` does not occur in `self`, `self` is set to the empty slice,
     *      and the entirety of `self` is returned.
     * @param self The slice to split.
     * @param needle The text to search for in `self`.
     * @return The part of `self` up to the first occurrence of `delim`.
     */
    function split(slice memory self, slice memory needle) internal pure returns (slice memory token) {
        split(self, needle, token);
    }

    /*
     * @dev Splits the slice, setting `self` to everything before the last
     *      occurrence of `needle`, and `token` to everything after it. If
     *      `needle` does not occur in `self`, `self` is set to the empty slice,
     *      and `token` is set to the entirety of `self`.
     * @param self The slice to split.
     * @param needle The text to search for in `self`.
     * @param token An output parameter to which the first token is written.
     * @return `token`.
     */
    function rsplit(slice memory self, slice memory needle, slice memory token) internal pure returns (slice memory) {
        uint ptr = rfindPtr(self._len, self._ptr, needle._len, needle._ptr);
        token._ptr = ptr;
        token._len = self._len - (ptr - self._ptr);
        if (ptr == self._ptr) {
            // Not found
            self._len = 0;
        } else {
            self._len -= token._len + needle._len;
        }
        return token;
    }

    /*
     * @dev Splits the slice, setting `self` to everything before the last
     *      occurrence of `needle`, and returning everything after it. If
     *      `needle` does not occur in `self`, `self` is set to the empty slice,
     *      and the entirety of `self` is returned.
     * @param self The slice to split.
     * @param needle The text to search for in `self`.
     * @return The part of `self` after the last occurrence of `delim`.
     */
    function rsplit(slice memory self, slice memory needle) internal pure returns (slice memory token) {
        rsplit(self, needle, token);
    }

    /*
     * @dev Counts the number of nonoverlapping occurrences of `needle` in `self`.
     * @param self The slice to search.
     * @param needle The text to search for in `self`.
     * @return The number of occurrences of `needle` found in `self`.
     */
    function count(slice memory self, slice memory needle) internal pure returns (uint cnt) {
        uint ptr = findPtr(self._len, self._ptr, needle._len, needle._ptr) + needle._len;
        while (ptr <= self._ptr + self._len) {
            cnt++;
            ptr = findPtr(self._len - (ptr - self._ptr), ptr, needle._len, needle._ptr) + needle._len;
        }
    }

    /*
     * @dev Returns True if `self` contains `needle`.
     * @param self The slice to search.
     * @param needle The text to search for in `self`.
     * @return True if `needle` is found in `self`, false otherwise.
     */
    function contains(slice memory self, slice memory needle) internal pure returns (bool) {
        return rfindPtr(self._len, self._ptr, needle._len, needle._ptr) != self._ptr;
    }

    /*
     * @dev Returns a newly allocated string containing the concatenation of
     *      `self` and `other`.
     * @param self The first slice to concatenate.
     * @param other The second slice to concatenate.
     * @return The concatenation of the two strings.
     */
    function concat(slice memory self, slice memory other) internal pure returns (string memory) {
        string memory ret = new string(self._len + other._len);
        uint retptr;
        assembly { retptr := add(ret, 32) }
        memcpy(retptr, self._ptr, self._len);
        memcpy(retptr + self._len, other._ptr, other._len);
        return ret;
    }

    /*
     * @dev Joins an array of slices, using `self` as a delimiter, returning a
     *      newly allocated string.
     * @param self The delimiter to use.
     * @param parts A list of slices to join.
     * @return A newly allocated string containing all the slices in `parts`,
     *         joined with `self`.
     */
    function join(slice memory self, slice[] memory parts) internal pure returns (string memory) {
        if (parts.length == 0)
            return "";

        uint length = self._len * (parts.length - 1);
        for(uint i = 0; i < parts.length; i++)
            length += parts[i]._len;

        string memory ret = new string(length);
        uint retptr;
        assembly { retptr := add(ret, 32) }

        for(uint i = 0; i < parts.length; i++) {
            memcpy(retptr, parts[i]._ptr, parts[i]._len);
            retptr += parts[i]._len;
            if (i < parts.length - 1) {
                memcpy(retptr, self._ptr, self._len);
                retptr += self._len;
            }
        }

        return ret;
    }
}

contract Orthos {

    using Strings for *;
    using Double for Double.double;

    struct SigData {
        address[] pList;
        bytes32[] hList;
        uint8[] vList;
        bytes32[] rList;
        bytes32[] sList;
    }

    struct Agent {
        bytes32 cypher;
        string report;
        SigData sd;
        uint score;
        bool scored;
        bool submitted;
        bool exists;
    }

    struct Query {
        address requester;
        string query;
        string[] allSignals;
        uint budget;
        address[] allAgents;
        string mech;
        bool scoringStarted;
        bool scoringFinished;
        uint totalScore;
    }

    struct Signal {
        bool exists;
        uint total;
    }

    mapping (address => uint) private rewardBalance;

    mapping (uint => mapping(string => Signal)) private signalSpace;

    mapping (uint => Query) private queries;

    mapping (uint => mapping(address => Agent)) private agents;

    mapping (uint => mapping(address => mapping(address => bool))) private usedAddr;

    uint[] public queryBag;
    uint public numQueries;
    mapping (uint => uint) private queryMap;
    mapping (string => uint) private str2id;
    uint private queryId;
    uint private PRECISION;
    int private ALPHA;
    uint private SECURITY_THRESHOLD;
    address private owner;

    modifier isParticipant(uint qid) {
        require(
            agents[qid][msg.sender].exists == true,
            "You are not a participant"
        ); _;
    }

    modifier isRequester(uint qid) {
        require(
            msg.sender == queries[qid].requester,
            "caller is not requester"
        ); _;
    }

    modifier hasScoringStarted(uint qid) {
        require(
            queries[qid].scoringStarted == true,
            "Scoring has not yet begun"
        ); _;
    }

    modifier hasScoringNotStarted(uint qid) {
        require(
            queries[qid].scoringStarted == false,
            "Scoring has already begun"
        ); _;
    }

    modifier hasScoringFinished(uint qid) {
        require(
            queries[qid].scoringFinished == true,
            "Scoring has already begun"
        ); _;
    }

    modifier isNotScored(uint qid) {
        require(
            agents[qid][msg.sender].scored == false,
            "You are already scored"
        ); _;
    }

    modifier isScored(uint qid) {
        require(
            agents[qid][msg.sender].scored == true,
            "You are not yet scored"
        ); _;
    }

    modifier queryEnded(uint qid) {
        require(
            queryMap[qid] == 0,
            "Query has not ended yet"
        ); _;
    }

    constructor() public {
        queryId = 0;
        numQueries = 0;
        PRECISION = 4;
        ALPHA = 1;
        SECURITY_THRESHOLD = 5;
        owner = msg.sender;
    }

    function updateParams (uint _precision, int _alpha, uint _secThreshold)
    public {
        require (owner == msg.sender, "Only owner can update the params");
        PRECISION = _precision;
        ALPHA = _alpha;
        SECURITY_THRESHOLD = _secThreshold;
    }

    function addQuery(string memory _query, string[] memory _allSignals,
                      string memory _mech)
    public payable {
        queryId++;
        numQueries++;
        queries[queryId].requester = msg.sender;
        queries[queryId].query = _query;
        queries[queryId].allSignals = _allSignals;
        string memory signal;
        for (uint i = 0; i < _allSignals.length; i++) {
            signal = _allSignals[i];
            signalSpace[queryId][signal].exists = true;
            signalSpace[queryId][signal].total = 0;
        }
        queries[queryId].budget = msg.value;
        queries[queryId].mech = _mech;
        if (numQueries > queryBag.length) {
            queryBag.push(queryId);
        } else {
            queryBag[numQueries-1] = queryId;
        }
        queryMap[queryId] = numQueries - 1;
        str2id[_query] = queryId;
        queries[queryId].scoringStarted = false;
        queries[queryId].scoringFinished = false;
    }

    function getNumQueries()
    public view returns (uint) {
        return queryBag.length;
    }

    function getQueryId(string memory _query)
    public view returns (uint) {
        return str2id[_query];
    }

    function getQuery(uint qid)
    public view returns (string memory) {
        return queries[qid].query;
    }

    function getAllQueries()
    public view returns (uint[] memory) {
        return queryBag;
    }

    function getAllSignals(uint qid)
    public view returns (string[] memory) {
        return queries[qid].allSignals;
    }

    function getNumSignals(uint qid)
    public view returns (uint) {
        return queries[qid].allSignals.length;
    }

    function getNumAgents(uint qid)
    public view returns (uint) {
        return queries[qid].allAgents.length;
    }

    function getBudget(uint qid)
    public view returns (uint) {
        return queries[qid].budget;
    }

    function submit(uint qid, bytes32 _cypher, address[] memory _pList, bytes32[] memory _hList,
                    uint8[] memory _vList, bytes32[] memory _rList, bytes32[] memory _sList)
    public {
        require(
            _pList.length == _hList.length && _pList.length == _vList.length,
            "No. of peer addresses does not match with no. of signatures or hashes"
        );
        require (
            _vList.length == _sList.length && _vList.length == _sList.length,
            "All the singature data is not of the same length"
        );

        agents[qid][msg.sender].cypher = _cypher;
        agents[qid][msg.sender].sd.pList  = _pList;
        agents[qid][msg.sender].sd.hList  = _hList;
        agents[qid][msg.sender].sd.vList  = _vList;
        agents[qid][msg.sender].sd.rList  = _rList;
        agents[qid][msg.sender].sd.sList  = _sList;
        agents[qid][msg.sender].exists = true;
        queries[qid].allAgents.push(msg.sender);
    }

    function reveal(uint qid, string memory _report, string memory _secret)
    public isParticipant(qid) queryEnded(qid) {
        require(
            signalSpace[qid][_report].exists == true,
            "invalid report"
        );
        require(
            agents[qid][msg.sender].submitted == false,
            "report already submitted"
        );
        require(
            keccak256(abi.encodePacked(_report, _secret)) == agents[qid][msg.sender].cypher,
            "incorrect cypher, report, secret combination"
        );
        //require(
            //verifyLocation(qid, msg.sender) == true,
            //"Failed to verify agent location"
        //);
        agents[qid][msg.sender].submitted = true;
        agents[qid][msg.sender].report = _report;
        signalSpace[qid][_report].total++;
    }

    function endQuery(uint qid)
    public isRequester(qid) {
        uint lastQuery      = queryBag[numQueries-1];
        uint index          = queryMap[qid];
        queryBag[index]     = lastQuery;
        queryMap[lastQuery] = index;
        queryMap[qid]       = 0;
        delete queryBag[numQueries-1];
        numQueries--;
    }

    function ptsc(bool isSame, uint signalTotal, uint totalReports)
    internal view returns (Double.double memory result) {
        if (isSame == true) signalTotal -= 1;
        Double.double memory rt       = Double.newDouble(int(totalReports - 2), 0, PRECISION);
        Double.double memory st       = Double.newDouble(int(signalTotal), 0, PRECISION);
        Double.double memory minusOne = Double.newDouble(-1, 0, PRECISION);
        Double.double memory dAlpha   = Double.newDouble(ALPHA, 0, PRECISION);
        if (isSame == true) {
            Double.double memory freq = rt.doubleDiv(st);
            Double.double memory temp = freq.doubleAdd(minusOne);
            result = dAlpha.doubleMult(temp);
        } else {
            result = dAlpha.doubleMult(minusOne);
        }
    }

    function rptsc(bool isSame, uint signalTotal, uint totalReports)
    internal view returns (Double.double memory result) {
        if (isSame == true) signalTotal -= 1;
        Double.double memory rt     = Double.newDouble(int(totalReports - 2), 0, PRECISION);
        Double.double memory st     = Double.newDouble(int(signalTotal), 0, PRECISION);
        Double.double memory dAlpha = Double.newDouble(ALPHA, 0, PRECISION);
        Double.double memory freq   = rt.doubleDiv(st);
        if ((freq.part == 0 && freq.f == 0) || isSame == true) {
            result = Double.newDouble(0, 0, PRECISION);
        } else {
            result = dAlpha.doubleDiv(freq);
        }
    }

    function startScoring(uint qid)
    public isRequester(qid) hasScoringNotStarted(qid) {
        queries[qid].scoringStarted = true;
    }

    function finishScoring(uint qid)
    public isRequester(qid) hasScoringStarted(qid) {
        queries[qid].scoringFinished = true;
    }

    function compareReports(string memory selfReport, string memory peerReport)
    public pure returns (bool) {
        Strings.slice memory sr = selfReport.toSlice();
        Strings.slice memory pr = peerReport.toSlice();
        return (sr.compare(pr) == 0) ? true : false;
    }

    function calcScore(uint qid)
    public {
        Agent memory agent      = agents[qid][msg.sender];
        uint randIndex          = ((now % 10 ** 2)*(queries[qid].allAgents.length-1))/100;
        Agent memory peer       = agents[qid][queries[qid].allAgents[randIndex]];
        uint signalTotal        = signalSpace[qid][agent.report].total;
        uint totalReports       = queries[qid].allAgents.length;
        bool isSame             = compareReports(agent.report, peer.report);
        Strings.slice memory ms = queries[qid].mech.toSlice();
        Double.double memory score;
        if (ms.compare("ptsc".toSlice()) == 0) {
            score = ptsc(isSame, signalTotal, totalReports);
        } else {
            score = rptsc(isSame, signalTotal, totalReports);
        }
        Double.double memory hundred = Double.newDouble(100, 0, 2);
        agents[qid][msg.sender].score = score.doubleMult(hundred).convert();
        queries[qid].totalScore += agents[qid][msg.sender].score;
        agents[qid][msg.sender].scored = true;
    }

    function getScore(uint qid)
    public view isParticipant(qid) isScored(qid) returns (uint) {
        return agents[qid][msg.sender].score;
    }

    function updateBalance(uint qid)
    public hasScoringFinished(qid) isParticipant(qid) isScored(qid) {
        Agent memory agent = agents[qid][msg.sender];
        rewardBalance[msg.sender] += (agent.score * queries[qid].budget) / queries[qid].totalScore;
    }

    function withdraw()
    public returns (uint) {
        uint amount = rewardBalance[msg.sender];
        rewardBalance[msg.sender] = 0;
        msg.sender.transfer(amount);
    }

    function getBalance()
    public view returns (uint) {
        return rewardBalance[msg.sender];
    }

    function recover(bytes32 hash, uint8 v, bytes32 r, bytes32 s)
    internal pure returns (address) {
        // EIP-2 still allows signature malleability for ecrecover(). Remove this possibility and make the signature
        // unique. Appendix F in the Ethereum Yellow paper (https://ethereum.github.io/yellowpaper/paper.pdf), defines
        // the valid range for s in (281): 0 < s < secp256k1n ÷ 2 + 1, and for v in (282): v ∈ {27, 28}. Most
        // signatures from current libraries generate a unique signature with an s-value in the lower half order.
        //
        // If your library generates malleable signatures, such as s-values in the upper range, calculate a new s-value
        // with 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141 - s1 and flip v from 27 to 28 or
        // vice versa. If your library also generates signatures with 0/1 for v instead 27/28, add 27 to v to accept
        // these malleable signatures as well.
        if (uint256(s) > 0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D576E7357A4501DDFE92F46681B20A0) {
            return address(0);
        }

        if (v != 27 && v != 28) {
            return address(0);
        }

        // If the signature is valid (and not malleable), return the signer address
        return ecrecover(hash, v, r, s);
    }

    function verifyLocation(uint qid, address agentAddr)
    internal returns (bool) {
        Agent memory agent = agents[qid][agentAddr];
        uint tempVal = 0;
        address peerAddr;
        bytes32 hash;
        uint8 v;
        bytes32 r;
        bytes32 s;
        for (uint i = 0; i < agent.sd.pList.length; i++) {
            peerAddr = agent.sd.pList[i];
            if (usedAddr[qid][agentAddr][peerAddr] == true) {
                continue;
            } else {
                usedAddr[qid][agentAddr][peerAddr] = true;
            }
            hash = keccak256(abi.encodePacked("\x19Ethereum Signed Message:\n32", agent.sd.hList[i]));
            v    = agent.sd.vList[i];
            r    = agent.sd.rList[i];
            s    = agent.sd.sList[i];
            if (peerAddr == recover(hash, v, r, s)) {
                tempVal++;
            }
        }
        return (tempVal >= SECURITY_THRESHOLD) ? true : false;
    }

}
