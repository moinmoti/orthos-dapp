solFile="$1.sol"
abifile="$1.abi"
binFile="$1.bin"

solc $solFile --bin --abi --optimize --overwrite -o output/

web3j solidity generate -b output/$binFile -a output/$abifile -p com.example.android.localbrain -o ../../../java/
