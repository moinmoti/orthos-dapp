/**
 *Submitted for verification at Etherscan.io on 2019-06-28
*/

pragma solidity >=0.4.22;
// pragma experimental ABIEncoderV2;

contract FaRM {

    struct Agent {
        bytes32 cypher;
        uint report;
        string LContext;
        uint reward;
        bool submitted;
        bool exists;
    }

    address requester;
    string public query;
    uint public numSignals;

    mapping (uint => uint) signalCount;
    string[] public signals;
    uint public budget;

    mapping (address => Agent) agents;
    uint public numAgents;
    
    modifier isParticipant() {
        require(
            agents[msg.sender].exists == true,
            "You are not a participant"
        ); _;    
    }

    constructor(string memory _query, uint _numSignals, uint _budget) public payable {
        requester = msg.sender;
        query = _query;
        numSignals = _numSignals;
        budget = _budget;
        numAgents = 0;
    }
    
    // function getSignals() view public returns (string[] memory) {
    //     return signals;
    // } 

    function addSignal(string memory name) public {
        require(
            msg.sender == requester,
            "caller is not requester"
        );
        signals.push(name);
    }

    function submit(string memory _context, bytes32 _cypher) public {
        agents[msg.sender].cypher = _cypher;
        agents[msg.sender].LContext = _context;
        agents[msg.sender].exists = true;
        numAgents++;
    }

    function reveal(uint _report, bytes32 _secret) 
        public
        isParticipant
    {
        require(
            _report < numSignals,
            "invalid report"
        );
        require(
            agents[msg.sender].submitted == false,
            "report already submitted"
        );
        require(
            keccak256(abi.encodePacked(_report, _secret)) == agents[msg.sender].cypher,
            "incorrect cypher, report, secret combination"
        );
        agents[msg.sender].submitted = true;
        agents[msg.sender].report = _report;
        signalCount[_report]++;
    }

    // function getReward(string)
}
